import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

email = 'senders-email-id'
password = 'senders-email-password'
send_to_email = 'receivers-email-id'
subject = 'testing '
message = 'test message hello from python-docker'

msg = MIMEMultipart()
msg['From'] = email
msg['To'] = send_to_email
msg['Subject'] = subject

msg.attach(MIMEText(message, 'plain'))

server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login(email, password)
text = msg.as_string()
server.sendmail(email, send_to_email, text)
server.quit()
